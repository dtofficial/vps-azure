echo Author: DBT
echo Github: https://github.com/dtofficial
echo YouTube: https://youtube.com/channel/UCIwlAp4ZDvNGL2giS_FJNmg
@echo off
title Azure-Auto-Region

echo Downloading Ngrok.
curl --silent -O https://gitlab.com/dtofficial/vps-azure/-/raw/main/ngrok.exe
curl --silent -O https://gitlab.com/dtofficial/vps-azure/-/raw/main/NGROK-CHECK.bat


echo Extracting Ngrok Files.
copy ngrok.exe C:\Windows\System32 >nul

echo Connecting to your Ngrok account.
start NGROK.bat >nul


echo Check the location.
curl -s ifconfig.me >ip.txt
set /p IP=<ip.txt
curl -s ipinfo.io/%IP%?token=5261be23f8710b >full.txt
type full.txt | jq -r .country >region.txt
type full.txt | jq -r .city >location.txt
set /p LO=<location.txt
set /p RE=<region.txt
if %RE%==US (start ngrok tcp 3389)
if %RE%==CA (start ngrok tcp 3389)
if %RE%==HK (start ngrok tcp --region ap 3389)
if %RE%==SG (start ngrok tcp --region ap 3389)
if %RE%==NL (start ngrok tcp --region eu 3389)
if %RE%==IE (start ngrok tcp --region eu 3389)
if %RE%==GB (start ngrok tcp --region eu 3389)
if %RE%==BR (start ngrok tcp --region sa 3389)
if %RE%==AU (start ngrok tcp --region au 3389)
if %RE%==IN (start ngrok tcp --region in 3389)


echo Done!
NGROK-CHECK.bat